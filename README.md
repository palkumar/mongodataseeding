* Checkout the code.
* Install [MongoDB](https://www.npmjs.com/package/mongodb) nodejs driver.

```npm install mongodb```

add -g at the end to have it installed globally.

* Update the JSON structure, DB Credentails and the collection name as needed.
* run below command

```node poDataSeeding.js```